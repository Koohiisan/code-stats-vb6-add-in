Attribute VB_Name = "modTimeZones"
Private Type SYSTEMTIME
    wYear                As Integer
    wMonth               As Integer
    wDayOfWeek           As Integer
    wDay                 As Integer
    wHour                As Integer
    wMinute              As Integer
    wSecond              As Integer
    wMilliseconds        As Integer
End Type
Private Type TIME_ZONE_INFORMATION
    Bias As Long
    StandardName(0 To 31) As Integer
    StandardDate As SYSTEMTIME
    StandardBias As Long
    DaylightName(0 To 31) As Integer
    DaylightDate As SYSTEMTIME
    DaylightBias As Long
End Type

Public Enum TIME_ZONE
    TIME_ZONE_ID_INVALID = 0
    TIME_ZONE_STANDARD = 1
    TIME_ZONE_DAYLIGHT = 2
End Enum

Private Declare Function GetTimeZoneInformation Lib "kernel32" (lpTimeZoneInformation As TIME_ZONE_INFORMATION) As Long
Private Declare Sub GetLocalTime Lib "KERNEL32.dll" (lpSystemTime As SYSTEMTIME)
Public Function GetCurrentISOTimestamp() As String
    Dim tzInfo As TIME_ZONE_INFORMATION
    Dim tzDST As TIME_ZONE
    Dim stNow As SYSTEMTIME
    Dim datDate As Date
    Dim intAddDST As Integer
    Dim intBias As Integer
    Dim strSign As String
    Dim intTZBias As Integer
    
    tzDST = GetTimeZoneInformation(tzInfo)
    
    GetLocalTime stNow
    With stNow
        datDate = DateSerial(.wYear, .wMonth, .wDay) + TimeSerial(.wHour, .wMinute, .wSecond)
    End With
   
    intTZBias = tzInfo.Bias
   
    If tzDST = TIME_ZONE_DAYLIGHT Then
        intAddDST = (tzInfo.DaylightBias / 60) * -1
    Else
        intAddDST = 0
    End If
    
    If intAddDST > 0 Then
        strSign = "+"
    Else
        strSign = "-"
    End If
    
    GetCurrentISOTimestamp = Format(datDate, "yyyy-MM-ddTHH:NN:SS") & strSign & Format(intTZBias / 60 + intAddDST, "0#") & ":00"
End Function
