Attribute VB_Name = "chilkatInit"
Function Init() As Boolean

    Dim success As Long
    
    ' if building this manually, you MUST supply your own license code here!
    ' (this one is meaningless and only enables a 30-day demo)
    chilkatCode = "TTDGCN.CB1092020_yANd8KcR9z2o"
    
    Dim chilkatInstance As New ChilkatGlobal
    success = chilkatInstance.UnlockBundle(chilkatCode)
    If (success <> 1) Then
        MsgBox chilkatInstance.LastErrorText
        Init = False
        Exit Function
    End If
    
    Init = True

End Function

