VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ISubclassEvent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'----- ISubclassEvent ---------------------------------------------------------------------
'  Ensure this class is named ISubclassEvent
'-----------------------------------------------------------------------------------------

Option Explicit

Public Enum enumSubclassActions
    scevForwardMessage = 0     ' continue the message down the subclassing chain
    scevSendToOriginalProc = 1 ' skip the chain & send message directly to original window procedure
    scevDoNotForwardEvent = -1 ' do not forward this message any further down the chain
End Enum

Public Function ProcessMessage(ByVal Key As String, ByVal hWnd As Long, ByVal Message As Long, _
                ByRef wParam As Long, ByRef lParam As Long, ByRef Action As enumSubclassActions, _
                ByRef WantReturnMsg As Boolean, ByVal ReturnValue As Long) As Long

' Key. The Key provided during the SubclassWindow() call
' hWnd. The subclassed window's handle
' Message. The message to process
' wParam & lParam. Message-specific values
' Action. Action to be taken after you process this message
' WantReturnMsg. Set to True if want to monitor the result after message completely processed
' ReturnValue. The final result of the message and passed only when WantReturnMsg = True

' Notes
'   WantReturnMsg. This parameter serves two purposes:
'   1) Indication whether this message is received BEFORE other subclassers have received
'       it or AFTER the last subclasser has processed the message.
'       If parameter = False, this is a BEFORE event
'       If parameter = True, this is an AFTER event
'   2) Allows you to request an AFTER event. Set parameter to True during the BEFORE event.
'   Parameter is ignored if Action is set to scevDoNotForwardEvent in the BEFORE event.
'   When WantReturnMsg is set to True, after the subclassing chain processes the
'       message, you will get a second event. The WantReturnMsg  parameter will be True
'       and the ReturnValue parameter will contain the final result. This is the AFTER event.

'   wParam & lParam can be changed by you. Any changes are forwarded down the chain as necessary

'   Key parameter, if set, is very useful if subclassing multiple windows at the same time.
'   All subclassed messages for the same object implementing this class receives all messages
'   for each subclassed window thru this same event. To make it simpler to determine which
'   hWnd relates to what type of window, the Key can be used.

'   The return value of this function is only used if Action is set to scevDoNotForwardEvent
End Function

