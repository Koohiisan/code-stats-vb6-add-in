VERSION 5.00
Begin VB.Form frmGraphics 
   Caption         =   "Form1"
   ClientHeight    =   3015
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   Icon            =   "frmGraphics.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3015
   ScaleWidth      =   4560
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer tmrHook 
      Enabled         =   0   'False
      Interval        =   2000
      Left            =   3840
      Top             =   2160
   End
   Begin VB.PictureBox picIcon 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   120
      Picture         =   "frmGraphics.frx":000C
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   0
      Top             =   240
      Width           =   495
   End
End
Attribute VB_Name = "frmGraphics"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Implements ISubclassEvent
Private Const WM_KEYUP = &H101
Private Sub cmdSubclass_Click()
    'SubclassWindow Text1.hWnd, Me, "txt1"
End Sub
Private Sub cmdUnSubclass_Click()
    'UnsubclassWindow Text1.hWnd, Me, "txt1"
End Sub
Public Function ISubclassEvent_ProcessMessage(ByVal Key As String, ByVal hWnd As Long, _
                    ByVal Message As Long, wParam As Long, lParam As Long, _
                    Action As enumSubclassActions, WantReturnMsg As Boolean, _
                    ByVal ReturnValue As Long) As Long

    Select Case Message
    Case WM_KEYUP
        'MsgBox wParam
        If (wParam >= 33 And wParam <= 46) Or _
           (wParam >= 16 And wParam <= 19) Or _
           (wParam >= 112 And wParam <= 123) Or _
           (wParam >= 144 And wParam <= 145) Or _
           (wParam = 19) Or _
           (wParam = 91) _
        Then Exit Function 'arrow keys and stuff
        numChars = numChars + 1
        totalNumChars = totalNumChars + 1
        
        With VBInstance
            With MenuButton
                .Caption = "&Code::Stats" & " (" & Format(totalNumChars, totalNumCharsMask) & ")"
            End With
        End With
        
        If numChars > 32 Then SendWork
    End Select
End Function

