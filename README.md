# Code::Stats Visual Basic 6.0 (VB6) Add-in

**[Luke Gerhardt](https://www.lukegerhardt.com/) @ 2019 Total Digital Consulting, LLC**


## Installation
1. Locate the [installer](https://bitbucket.org/Koohiisan/code-stats-vb6-add-in/src/master/release/codestats-vb6-add-in.exe) in the release folder, and choose 'view raw' in order to download it in your browser.
2. Run the installer and follow the prompts to install the Code::Stats VB6 Add-in.
3. Open the Visual Basic 6 IDE.[^1]  You will be prompted for your Code::Stats machine key.
4. Start coding!

## Notes
* It is not necessary to download or use the source code at all, if all you are interested in is the actual add-in.
* Coding XP is counted until it reaches 32 or higher.  At that time the total amount is sent to the Code::Stats API.  Remaining XP which has not yet reached that threshold will be sent upon exiting the IDE.

## Operating System Support[^2]
* Windows 10
* Windows 7
* Windows XP ` * NEW! * `
* Windows 2000 ` * NEW! * `

## Notes on Building From Source
* The MSXML component is no longer supported since the Code::Stats API now requires TLS1.2 or higher to connect (as of at least September 2019) and this component does not properly support that security protocol under Windows versions prior to 10.
* To enable backwards compatibility for Windows XP and 7, I have replaced the MSXML component with a paid, ActiveX component from [Chilkat Software](https://www.chilkatsoft.com/downloads_ActiveX.asp).
* If you wish to download and build this add-in from source, you will need to follow the directions on Chilkat Software's site to install the ActiveX component and license it properly.
* I have left the MSXML component bits in the code, and gated them off with boolean contants so that users who only care about Windows 10 can disable the Chilkat component and use the previous MSXML component.

## Getting Help
You can report issues in a few ways (in descending order of speed and effectiveness):

* Use the built-in [issue tracker](https://bitbucket.org/Koohiisan/code-stats-vb6-add-in/issues?status=new&status=open) here on Bitbucket
* Use the [contact form](https://www.lukegerhardt.com/contact) on my website
* Send me a [direct message](https://twitter.com/koohiisan) on Twitter

*Windows, Visual Basic, Visual Basic 6.0, VB6, et al are copyrighted by Microsoft Corp.*

[^1]: If you experience any issues such as the add-in repeatedly prompting for your machine key, please run the IDE once as administrator, so that it will have permissions to save this data.
[^2]: Check [here](http://blog.danbrust.net/2015/09/14/installing-visual-basic-studio-6-on-windows-10/) for an excellent resource on installing the VB6 IDE on modern versions of Windows.
