Attribute VB_Name = "mSubclass"
Option Explicit

Public VBInstance As VBIDE.VBE
Attribute VBInstance.VB_VarDescription = "VB itself"
Global MenuButton As Office.CommandBarButton

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hWnd As Long, ByVal msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

Private hWndCodeWindow As Long
Public PrevProcPtr(255) As Long
Public PrevProcPtrMDIParent As Long

Public Declare Function FindWindowEx Lib "user32" Alias "FindWindowExA" (ByVal hWnd1 As Long, ByVal hWnd2 As Long, ByVal lpsz1 As String, ByVal lpsz2 As String) As Long
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Public Const IDX_WINDOWPROC As Long = -4
Attribute IDX_WINDOWPROC.VB_VarDescription = "Pointer into window class properties"

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Private Const WM_KEYUP As Long = &H101
Const WM_MDIACTIVATE = &H222
Const WM_MDICREATE = &H220
Const WM_MDIGETACTIVE = &H229
Const WM_DESTROY = &H2

Global numChars As Long
Global totalNumChars As Long
Global booUnhookedMDIParent As Boolean
Public Const totalNumCharsMask = "###,###,###,##0"

Global HookedWindows(255) As Long
Global ignoredWindows(32767) As Long
Global apiToken As String

Global chilkatCode As String
Const HTTP_SEND_MODE_ASYNC = True
Const HTTP_SUPPORT_TLS12 = True

Public Function CodeWindowProc(ByVal hWnd As Long, ByVal nMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    Dim ii As Long

    CodeWindowProc = 0
    
    If nMsg = WM_KEYUP Then
        'MsgBox wParam
        If (wParam >= 33 And wParam <= 46) Or _
           (wParam >= 16 And wParam <= 19) Or _
           (wParam >= 112 And wParam <= 123) Or _
           (wParam >= 144 And wParam <= 145) Or _
           (wParam = 19) Or _
           (wParam = 91) _
        Then Exit Function 'arrow keys and stuff
        numChars = numChars + 1
        totalNumChars = totalNumChars + 1
    
        With VBInstance
            With MenuButton
                .Caption = "&Code::Stats" & " (" & Format(totalNumChars, totalNumCharsMask) & ")"
            End With
        End With
    
        If numChars > 32 Then SendWork
        
    ElseIf nMsg = WM_DESTROY Then
        For ii = 0 To 255
            If HookedWindows(ii) = hWnd Then
                SetWindowLong hWnd, IDX_WINDOWPROC, PrevProcPtr(ii)
                HookedWindows(ii) = 0
            End If
        Next ii
    End If
        
        For ii = 0 To 255
            If HookedWindows(ii) = hWnd Then
                    CodeWindowProc = CallWindowProc(PrevProcPtr(ii), hWnd, nMsg, wParam, lParam)
            End If
        Next ii

End Function
Public Function MDIParentProc(ByVal hWnd As Long, ByVal nMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

    Dim booIgnore As Boolean
    Dim i As Integer
    MDIParentProc = 0
    
    Select Case nMsg
    Case Is = WM_MDIGETACTIVE
        'MsgBox "WM message caught: wParam: " & wParam & "; lParam: " & lParam
        'UpdateHooks
    Case Is = WM_MDIACTIVATE
        'MsgBox "WM message caught: wParam: " & wParam & "; lParam: " & lParam
        For i = 0 To 32767
            If wParam = ignoredWindows(i) Then booIgnore = True
        Next i
        If Not booIgnore Then UpdateHooks
    Case Is = WM_MDICREATE
        'UpdateHooks
    End Select
    
    MDIParentProc = CallWindowProc(PrevProcPtrMDIParent, hWnd, nMsg, wParam, lParam)

End Function
Public Function SendWork()

    If HTTP_SUPPORT_TLS12 Then
    
        If HTTP_SEND_MODE_ASYNC Then
        
            SendWorkTLS12Aync
            Exit Function
    
        Else
        
            SendWorkTLS12
            Exit Function
    
        End If
        
    End If
    
    Dim sUrl As String
    Dim response As String
    Dim jsonPulse As String
    Dim xmlHttp
    
    jsonPulse = "{""coded_at"": """ & GetCurrentISOTimestamp & """,""xps"": [{""language"": ""VB6"", ""xp"": " & numChars & "}]}"
    sUrl = "https://codestats.net/api/my/pulses"
    
    Set xmlHttp = New ServerXMLHTTP
    xmlHttp.Open "POST", sUrl, False
    xmlHttp.SetRequestHeader "Content-Type", "application/json"
    xmlHttp.SetRequestHeader "X-API-Token", apiToken
    xmlHttp.SetRequestHeader "User-Agent", "code-stats-vb6"
    xmlHttp.SetRequestHeader "Accept", "*/*"
    xmlHttp.send (jsonPulse)
    
    response = xmlHttp.responseText
    Set xmlHttp = Nothing
    
    If response = "{""ok"":""Great success!""}" Then
        numChars = 0
    Else
        MsgBox "CodeStats Addin: Error on pulse!" & Chr(13) & Chr(13) & response
    End If
    Exit Function
    
Handler:
    MsgBox "SendWork Error: " & Err.Description
End Function
Public Function SendWorkTLS12()
    Dim sUrl As String
    Dim response As String
    Dim jsonPulse As String
    Dim http As New ChilkatHttp
    Dim req As New ChilkatHttpRequest
    Dim resp As ChilkatHttpResponse
    Dim success As Long
    Dim initResult As Boolean
        
    If apiToken = "" Then Exit Function
    
    initResult = chilkatInit.Init()
    
    If Not initResult Then
        MsgBox "Chilkat HTTP component is not licensed for this environment!"
        Exit Function
    End If
    
    jsonPulse = "{""coded_at"": """ & GetCurrentISOTimestamp & """,""xps"": [{""language"": ""VB6"", ""xp"": " & numChars & "}]}"
    sUrl = "https://codestats.net/api/my/pulses"
    
    http.SetRequestHeader "Content-Type", "application/json"
    http.SetRequestHeader "X-API-Token", apiToken
    http.SetRequestHeader "User-Agent", "code-stats-vb6"
    http.SetRequestHeader "Accept", "*/*"
    
    req.HttpVerb = "POST"
    req.contentType = "application/json"
    req.SendCharset = 1
    req.Charset = "utf-8"
    
    Set resp = http.PostJson(sUrl, jsonPulse)
    If (http.LastMethodSuccess <> 1) Then
        MsgBox "CodeStats Addin: Error on pulse!" & Chr(13) & Chr(13) & http.LastErrorText
    Else
        numChars = 0
    End If
    
    Exit Function
    
Handler:
    MsgBox "SendWorkTLS12 Error: " & Err.Description
End Function
Public Function SendWorkTLS12Aync()
    Dim sUrl As String
    Dim response As String
    Dim jsonPulse As String
    Dim http As New ChilkatHttp
    Dim task As ChilkatTask
    Dim resp As New ChilkatHttpResponse
    Dim statusCode As Long
    Dim json As New ChilkatJsonObject
    Dim initResult As Boolean
    Dim success As Long
    
    If apiToken = "" Then Exit Function
    
    initResult = chilkatInit.Init()
    
    If Not initResult Then
        MsgBox "Chilkat HTTP component is not licensed for this environment!"
        Exit Function
    End If
    
    jsonPulse = "{""coded_at"": """ & GetCurrentISOTimestamp & """,""xps"": [{""language"": ""VB6"", ""xp"": " & numChars & "}]}"
    sUrl = "https://codestats.net/api/my/pulses"

    http.SetRequestHeader "Content-Type", "application/json"
    http.SetRequestHeader "X-API-Token", apiToken
    http.SetRequestHeader "User-Agent", "code-stats-vb6"
    http.SetRequestHeader "Accept", "*/*"

    Set task = http.PostJson2Async(sUrl, "application/json", jsonPulse)
    
    If (http.LastMethodSuccess <> 1) Then
        MsgBox "CodeStats Addin: Error on pulse!" & Chr(13) & Chr(13) & http.LastErrorText
    Else
        numChars = 0
    End If

    success = task.Run()
    If (Not success) Then
        ' MsgBox "SendWorkTLS12Async error: " & task.LastErrorText
    
        Exit Function
    End If

    Do While task.Finished <> 1
        task.SleepMs 100
    Loop

    If (task.StatusInt <> 7) Then
        MsgBox "SendWorkTLS12Async task did not complete: " + task.Status
        Exit Function
    End If

    If (task.TaskSuccess = 0) Then
        MsgBox "SendWorkTLS12Async error: " & "The underlying task failed, and there is no HTTP response object: " & task.ResultErrorText
        Exit Function
    End If

    success = resp.LoadTaskResult(task)
    
    statusCode = resp.statusCode
    MsgBox "Response status code = " & statusCode

    success = json.Load(resp.BodyStr)
    json.EmitCompact = 0
    Debug.Print "JSON Response Body:"
    Debug.Print json.Emit()
End Function
Public Function HookAllWindows()
    'Exit Function
    Dim currentWindow As Long
    Dim currentMDIWindow As Long
    Dim currentWindowCaption As String
    Dim i As Integer
    Dim ii As Integer
    
    On Error GoTo Handler
        
    With VBInstance
        
        'MsgBox "Characters typed: " & numChars
        currentMDIWindow = FindWindowEx(.MainWindow.hWnd, 0&, "MDIClient", vbNullString)
    
        For i = 1 To .Windows.Count

            For ii = 0 To 255
                If .Windows.Item(i).Type = vbext_wt_CodeWindow Then
                    currentWindowCaption = .Windows.Item(i).Caption
                    currentWindow = FindWindowEx(currentMDIWindow, 0&, "VbaWindow", currentWindowCaption)
                    'MsgBox "Found code window: " & currentWindowCaption & " (hWnd: " & currentMDIWindow & "/" & currentWindow & ")"
                    If currentWindow = ignoredWindows(ii) Then Exit For
                    If currentWindow = HookedWindows(ii) Then Exit For
                    If HookedWindows(ii) = 0 Then
                        PrevProcPtr(ii) = SetWindowLong(currentWindow, IDX_WINDOWPROC, AddressOf CodeWindowProc)
                        HookedWindows(ii) = currentWindow
                        'MsgBox "Should be hooked up."
                        Exit For
                    End If
                    
                Else
                
                    ignoredWindows(ii) = currentWindow
            
                End If
            Next ii
                        
        Next i
        
    End With
    Exit Function
    
Handler:
    MsgBox "Error hooking up handlers to all windows: " & Err.Description & " " & Erl & " i:" & i & " ii: " & ii

End Function
Function UnHookAllWindows()
    'Exit Function
    On Error GoTo Unhook_Handler
    Dim i As Long
    Dim ii As Long
           
    With VBInstance
                
        For i = 1 To .Windows.Count

            For ii = 0 To 255
                If .Windows.Item(i).hWnd = HookedWindows(ii) Then
                    'UnsubclassWindow .Windows.Item(i).hWnd, frmGraphics
                    UnsubclassWindowByIndex ii
                    HookedWindows(ii) = 0
                End If
            Next ii
                        
        Next i
    End With
    Exit Function
Unhook_Handler:
    MsgBox Err.Description, vbOKOnly, Err.Number
End Function
Public Sub UnhookMDIParent()
    Dim currentMDIWindow
    If Not booUnhookedMDIParent Then
        booUnhookedMDIParent = True
        currentMDIWindow = FindWindowEx(VBInstance.MainWindow.hWnd, 0&, "MDIClient", vbNullString)
        SetWindowLong currentMDIWindow, IDX_WINDOWPROC, PrevProcPtrMDIParent
    End If
End Sub
Public Sub UpdateHooks()
    UnHookAllWindows
    HookAllWindows
End Sub

